from LambdaPage import LambdaPage
from bs4 import BeautifulSoup
import boto3
from io import BytesIO, StringIO
from requests_toolbelt.multipart import decoder
import os
import base64
from traceback import format_exc


def _get_bucket():
    bucket_name = os.environ['BUCKET_NAME'] if 'BUCKET_NAME' in os.environ else 'pentair-pypi'
    print('Using S3 bucket: %s' % bucket_name)
    return boto3.resource('s3').Bucket(bucket_name)


def get_index(event):
    """
        GET /pypi
    :param event:
    :return: html showing default welcome page with links to simple and package indices
    """
    with open('html/index.html') as f:
        html = BeautifulSoup(f.read(), 'html.parser')
    return str(html.prettify())


def get_simple_index(event):
    """
        GET /pypi/simple
    :param event:
    :return: html listing of available package names, without listing specific package files or version numbers.
    """
    objects = _get_bucket().objects.all()
    package_names = sorted(list(set([ob.key.split('/')[0] for ob in objects])))
    with open('html/simple_index.html') as f:
        html = BeautifulSoup(f.read(), 'html.parser')
    for package_name in package_names:
        a = html.new_tag('a')
        a.attrs['href'] = '/pypi/simple/%s/' % package_name
        a.append(package_name)
        html.body.append(a)
        html.body.append(html.new_tag('br'))
    return str(html.prettify())


def get_simple_package(event):
    """
        GET /pypi/simple/{package}
    :param event:
    :return: html listing of all package files corresponding to the provided package name
    """
    package_name = event['pathParameters']['package'].lower()
    objects = _get_bucket().objects.filter(Prefix=package_name)
    with open('html/simple_package.html') as f:
        html = f.read().replace('{{package}}', package_name)
    html = BeautifulSoup(html, 'html.parser')
    for ob in objects:
        package = ob.key.split('/')[-1]
        if len(package) == 0:
            continue
        etag = ob.e_tag
        a = html.new_tag('a')
        a.attrs['href'] = '/pypi/package/%s#md5=%s' % (package, etag)
        a.append(package)
        html.body.append(a)
        html.body.append(html.new_tag('br'))
    return str(html.prettify())


def get_package_index(event):
    """
        GET /pypi/packages
    :param event:
    :return: flat html listing of all package files available on the server
    """
    objects = _get_bucket().objects.all()
    with open('html/package_index.html') as f:
        html = BeautifulSoup(f.read(), 'html.parser')
    for ob in objects:
        package = ob.key.split('/')[-1]
        etag = ob.e_tag
        a = html.new_tag('a')
        a.attrs['href'] = '/pypi/package/%s#md5=%s' % (package, etag)
        a.append(package)
        html.body.append(a)
        html.body.append(html.new_tag('br'))
    return str(html.prettify())


def get_package(event):
    """
        GET /pypi/packages/{package}
    :param event:
    :return: Binary stream of a given package file
    """
    package = event['pathParameters']['package']
    prefix = package.split('-')[0].lower()
    key = '/'.join((prefix, package))
    obj = BytesIO()
    _get_bucket().download_fileobj(key, obj)
    obj.seek(0)
    return obj.read()


def upload_package(event):
    """
        POST /pypi
        Upload a given package file provided using a multipart-form
    :param event:
    :return: null
    """
    form_raw = StringIO(event['body'])
    form_raw.seek(0)
    md = decoder.MultipartDecoder(form_raw.read(), event['headers']['content-type'])
    form = {}
    for part in md.parts:
        key = part.headers[b'Content-Disposition'].decode().split('=')[1].replace('"', '')
        val = part.content.decode() if 'content;' not in key else part.content
        form[key] = val
    file_name = '-'.join(map(str, (form['name'], form['version'], 'none', 'any'))) + '.whl'
    key = '/'.join((form['name'].lower(), file_name))
    _get_bucket().put_object(Key=key, Body=form['content; filename'])


def create_lambda_page():
    page = LambdaPage()
    page.add_endpoint('get', '/pypi/simple/{package}', get_simple_package, 'text/html')
    page.add_endpoint('get', '/pypi/packages/{package}', get_package, 'application/octet-stream')
    page.add_endpoint('get', '/pypi/packages', get_package_index, 'text/html')
    page.add_endpoint('get', '/pypi/simple', get_simple_index, 'text/html')
    page.add_endpoint('get', '/pypi', get_index, 'text/html')
    page.add_endpoint('post', '/pypi', upload_package, 'text/html')
    return page


def auth(event, context):
    """
    Basic auth is provided using API Keys. By default the serverless config creates an API Key called 'pypi', but any
    valid name:key pair can be used here
    :param event:
    :param context:
    :return:
    """
    event['headers'] = {k.lower(): v for k, v in event['headers'].items()}
    if 'authorization' not in event['headers']:
        raise Exception('Missing authorization')
    b64_token = event['headers']['authorization'].split(' ')[-1]
    username, token = base64.b64decode(b64_token).decode("utf-8").split(':')
    client = boto3.client('apigateway')
    response = client.get_api_keys(nameQuery=username, includeValues=True)
    if len(response['items']) != 1:
        print("Couldn't find key")
        raise Exception('Unauthorized')
    if response['items'][0]['value'] != token:
        print("Key value mismatch")
        raise Exception('Unauthorized')


def lambda_handler(event, context):
    print(event)
    try:
        auth(event, context)
    except:
        ''' TODO: API Gateway currently doesn't "support" the WWW-Authenticate response header. It gets re-mapped to
            x-amzn-remapped-www-authenticate. So prompting for user login should be done some other way.
        '''
        return {'statusCode': 401, 'headers': {'WWW-Authenticate': 'Basic'}, 'body': 'Unauthorized: %s' % format_exc()}

    try:
        return create_lambda_page().handle_request(event)
    except:
        return {'statusCode': 500, 'body': format_exc()}


if __name__ == '__main__':
    create_lambda_page().start_local()
