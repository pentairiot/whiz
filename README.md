# Whiz

A personal serverless PyPi server using AWS S3, Lambda, and API Gateway, deployed using the [Serverless Framework](https://serverless.com)

## Endpoints

### Welcome page

> GET /pypi

Returns html showing default welcome page with links to simple and package indices

### Simple Index

> GET /pypi/simple

Returns html listing of available package names, without listing specific package files or version numbers.

### Simple package listing

> GET /pypi/simple/{package-name}

Returns html listing of all package files corresponding to the provided package name.


### Package Index

> GET /pypi/packages

Returns flat html listing of all package files available on the server.


### Download package file

> GET /pypi/packages/{package-file}

Returns a binary stream of a given package file


### Upload package

> POST /pypi

Upload a given package file provided via multipart-form. The form should define at least 'name' and 'version' along with the binary content of the package.


